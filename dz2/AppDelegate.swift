//
//  AppDelegate.swift
//  dz2
//
//  Created by Даниил Карпитский on 1/25/22.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    
    let goverment = Goverment()
    let doctor = Doctor()
    let buisnessman = Businessman()
    let pensioner = Pensioner()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        let doctor1 = doctor
        doctor1.name = "Igor"
        
        let buisnessman1 = buisnessman
        buisnessman1.name = "Michael"
        
        let pensioner1 = pensioner
        pensioner1.name = "mr. Biden"
        
        
//taxes
        goverment.taxLevel = -4
        goverment.setTaxLevel(taxLevel: goverment.taxLevel)
//salary
        goverment.salary = 800
        goverment.setSalary(salary: goverment.salary)
//pension
        goverment.pension = 50
        goverment.setPension(pension: goverment.pension)
//average price
        goverment.averagePrice = 60
        goverment.setAveragePrice(averagePrice: goverment.averagePrice)
        
        

        
        
        return true
        
    }
    
// MARK: Notifications
    
         // селектор
    
    
    
    // MARK: UISceneSession Lifecycle
    
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    
    
}
