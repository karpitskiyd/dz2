//
//  Businessman.swift
//  dz2
//
//  Created by Даниил Карпитский on 1/28/22.
//

import Foundation
class Businessman{
    var name: String = ""
    var taxLevel: Float = 0.0
    var averagePrice: Float = 0.0
    let goverment = Goverment()
    
    init(){
        // Subscribe on notification
        NotificationCenter.default.addObserver(self, selector: #selector(taxLevelDidChange), name: Notification.Name(goverment.taxLevelDidChangeNotification), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(averagePriceChangedNotification), name:    Notification.Name(goverment.averagePriceDidChangeNotification), object: nil)
    }
    
    @objc func taxLevelDidChange(notification: Notification){
        print("Government Notification:", notification.userInfo as AnyObject)
        let value = notification.userInfo![goverment.taxLevelUserInfoKey]        //here's new taxLevel value
        let newTaxLevel = value as! Float
        if newTaxLevel < taxLevel {
            print("\(self.name) is happy")
        }
        else {
            print("Buisnessman \(self.name) is angry")
        }
        self.taxLevel = newTaxLevel
        print("")
    }
    
    @objc func averagePriceChangedNotification(notification: Notification){
        print("Government notification:",notification.userInfo as AnyObject )
        let value = notification.userInfo![goverment.averagePriceUserInfoKey] //here's new average price value
        let newAveragePrice = value as! Float
        if newAveragePrice > averagePrice {
            print("Buisnessman \(self.name) doesn't eat meat anymore")
        }
        else {
            print("Buisnessman \(self.name) laught like a baby")
        }
        self.averagePrice = newAveragePrice
        print("")
    }
    
    
}
