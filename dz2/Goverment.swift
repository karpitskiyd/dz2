//
//  Goverment.swift
//  dz2
//
//  Created by Даниил Карпитский on 1/25/22.
//

import Foundation
class Goverment{
   
let taxLevelDidChangeNotification = "taxLevelDidChangeNotification"
let salaryDidChangeNotification = "salaryDidChangeNotification"
let pensionDidChangeNotification = "pensionDidChangeNotification"
let averagePriceDidChangeNotification = "averagePriceDidChangeNotification"

let taxLevelUserInfoKey = "taxLevelUserInfoKey"
let salaryUserInfoKey = "salaryUserInfoKey"
let pensionUserInfoKey = "pensionUserInfoKey"
let averagePriceUserInfoKey = "averagePriceUserInfoKey"
    
var taxLevel: Float = 40
var salary: Float = 20
var pension: Float = 30
var averagePrice: Float = 50

    func setTaxLevel(taxLevel: Float){
        self.taxLevel = taxLevel
          let dictionary = [taxLevelUserInfoKey : taxLevel]
          NotificationCenter.default.post(name: Notification.Name(taxLevelDidChangeNotification), object: nil, userInfo: dictionary)
        
    }
    
    func setSalary(salary: Float){
        self.salary = salary
          let dictionary = [salaryUserInfoKey : salary]
          NotificationCenter.default.post(name: Notification.Name(salaryDidChangeNotification), object: nil, userInfo: dictionary)
        
    }
    
    func setPension(pension: Float){
        self.pension = pension
          let dictionary = [pensionUserInfoKey : pension]
          NotificationCenter.default.post(name: Notification.Name(pensionDidChangeNotification), object: nil, userInfo: dictionary)

    }
    
    func setAveragePrice(averagePrice: Float){
        self.averagePrice = averagePrice
          let dictionary = [averagePriceUserInfoKey : averagePrice]
          NotificationCenter.default.post(name: Notification.Name(averagePriceDidChangeNotification), object: nil, userInfo: dictionary)
        
    }
    
}
