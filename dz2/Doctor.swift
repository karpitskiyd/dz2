//
//  Doctor.swift
//  dz2
//
//  Created by Даниил Карпитский on 1/28/22.
//

import Foundation
class Doctor {
    var salary: Float = 0.0
    var averagePrice: Float = 0.0
    var name: String = ""
    let goverment = Goverment()
    
    
    init(){
        NotificationCenter.default.addObserver(self, selector: #selector(salaryChangedNotification), name:    Notification.Name(goverment.salaryDidChangeNotification), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(averagePriceChangedNotification), name:    Notification.Name(goverment.averagePriceDidChangeNotification), object: nil)
        
        
    }
    // MARK: Notifications
    
    @objc func salaryChangedNotification(notification: Notification){
        print("Government notification:",notification.userInfo as AnyObject )
        let value = notification.userInfo![goverment.salaryUserInfoKey] //here's new salary value
        let newSalary = value as! Float
        if newSalary > salary {
            print("dr.\(self.name) is happy")
        }
        else {
            print("dr.\(self.name) is angry")
        }
        self.salary = newSalary
        print("")
    }
    
    @objc func averagePriceChangedNotification(notification: Notification){
        print("Government notification:",notification.userInfo as AnyObject )
        let value = notification.userInfo![goverment.averagePriceUserInfoKey] //here's new average price value
        let newAveragePrice = value as! Float
        if newAveragePrice > averagePrice {
            print("dr.\(self.name) doesn't eat meat anymore")
        }
        else {
            print("dr.\(self.name) laught like a baby")
        }
        self.averagePrice = newAveragePrice
        print("")
    }
}

