//
//  Pensioner.swift
//  dz2
//
//  Created by Даниил Карпитский on 1/28/22.
//

import Foundation
class Pensioner{
    var name: String = ""
    var pension: Float = 0.0
    var averagePrice: Float = 0.0
    let goverment = Goverment()
    
    init(){
        NotificationCenter.default.addObserver(self, selector: #selector(govermentNotification), name:    Notification.Name(goverment.pensionDidChangeNotification), object: nil)  // Subscribe on notification
        
        NotificationCenter.default.addObserver(self, selector: #selector(averagePriceChangedNotification), name:    Notification.Name(goverment.averagePriceDidChangeNotification), object: nil)
    }
    
    @objc func govermentNotification(notification: Notification){
        print("Government Notification:", notification.userInfo as AnyObject)
        let value = notification.userInfo![goverment.pensionUserInfoKey]        //here's new taxLevel value
        let newPension = value as! Float
        if newPension > pension {
            print("Pensioner \(self.name) is happy")
        }
        else {
            print("Pensioner \(self.name) is angry")
        }
        self.pension = newPension
        print("")
    }
    
    @objc func averagePriceChangedNotification(notification: Notification){
        print("Government notification:",notification.userInfo as AnyObject )
        let value = notification.userInfo![goverment.averagePriceUserInfoKey] //here's new average price value
        let newAveragePrice = value as! Float
        if newAveragePrice > averagePrice {
            print("Pensioner \(self.name) doesn't eat meat anymore")
        }
        else {
            print("Pensioner \(self.name) laught like a baby")
        }
        self.averagePrice = newAveragePrice
        print("")
    }
}
